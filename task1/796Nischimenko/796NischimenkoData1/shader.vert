#version 330
uniform mat4 viewMatrix;
uniform mat4 projMatrix;
uniform mat4 modelMatrix;

layout(location = 0) in vec3 vertex;
layout(location = 1) in vec3 norm;
layout(location = 2) in vec2 texture_coord;

out vec3 out_norm;
out vec2 texture_coord_out;

void main() {
    gl_Position = projMatrix * viewMatrix * modelMatrix * vec4(vertex, 1.0);
    out_norm = norm;
    texture_coord_out = texture_coord;
}