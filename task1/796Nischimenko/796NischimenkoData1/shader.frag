#version 330

uniform sampler2D sampler;

out vec4 color;

in vec2 texture_coord_out;
in vec3 out_norm;

void main () {
    color = vec4(texture(sampler, texture_coord_out).rgb, 1);
//    color = vec4(out_norm * 0.5 + 0.5, 1);
}