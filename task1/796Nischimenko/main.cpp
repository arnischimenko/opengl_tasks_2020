#include <iostream>
#include <array>
#include "common/Application.hpp"
#include "common/ShaderProgram.hpp"
#include "common/Mesh.hpp"
#include "common/Texture.hpp"


#define GLM_ENABLE_EXPERIMENTAL
using namespace glm;

struct Triangle {
    Triangle(const std::array<vec3, 3>& points):
        points(points) {}

    std::array<vec3, 3> points;
};

struct Rectangle {
//    Rectangle(const std::array<vec3, 4> &_points):
//        points(_points) {};

    std::array<vec3, 4> points;
    vec3 normal;


    std::vector<vec3> getPoints() {
        // 1 2
        // 0 3
        return std::vector<vec3> {points[0], points[1], points[2], points[0], points[2], points[3]};
    }

    std::vector<vec2> getTextureCoords() {
        return std::vector<vec2> {
            {0,0}, {0,1}, {1,1},
            {0,0}, {1,1}, {1,0}};
    }
};

struct Cube {
    Cube(int size, vec3 where) {
        facets[0] = Rectangle{
            where + vec3{0,0,0},
            where + vec3{0, size,0},
            where + vec3{size, size,0},
            where + vec3{size,0,0},
            vec3{0,0,-1}
        };
        facets[1] = Rectangle{
            where + vec3{0,0,0},
            where + vec3{0, 0,size},
            where + vec3{0, size,size},
            where + vec3{0,size,0},
            vec3{-1,0,0}
        };
        facets[2] = Rectangle{
            where + vec3{0,0,0},
            where + vec3{0,0,size},
            where + vec3{size, 0,size},
            where + vec3{size, 0,0},
            vec3{0,-1,0}
        };
        facets[3] = Rectangle{
            where + vec3{size,0,0},
            where + vec3{size, 0,size},
            where + vec3{size, size,size},
            where + vec3{size,size,0},
            vec3{1,0,0}
        };
        facets[4] = Rectangle{
            where + vec3{0,0,size},
            where + vec3{0, size,size},
            where + vec3{size, size,size},
            where + vec3{size,0,size},
            vec3{0,0,1},
        };
        facets[5] = Rectangle{
            where + vec3{0,size,0},
            where + vec3{0, size,size},
            where + vec3{size, size,size},
            where + vec3{size,size,0},
            vec3{0,1,0},
        };

    }
    std::array<Rectangle, 6> facets;
};


class MyApplication: public Application {
private:
    void draw() override {
        Application::draw();
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);
        glViewport(0, 0, width, height);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        _shader->use();

        glActiveTexture(GL_TEXTURE0);
        glBindSampler(0, _sampler);
        _texture->bind();

        _shader->setIntUniform("sampler", 0);

        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projMatrix", _camera.projMatrix);
        _shader->setMat4Uniform("modelMatrix", _mesh->modelMatrix());
        _mesh->draw();
    }

    void makeScene() override {
        Application::makeScene();
        _cameraMover = std::make_shared<FreeCameraMover>();
        _shader = std::make_shared<ShaderProgram>("796NischimenkoData1/shader.vert", "796NischimenkoData1/shader.frag");

        std::vector<std::vector<int>> map {
                {1, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1},
                {1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 1, 1, 1, 0},
                {1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 0, 0, 1, 0, 0, 1, 0, 1, 1, 0, 1, 0, 1, 0},
                {0, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 0, 0, 1, 0, 1, 1},
                {1, 1, 0, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 1, 1, 1, 0, 0, 1, 1, 0, 1, 0, 0, 0},
                {1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0},
                {1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1},
                {1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1},
                {0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 1},
                {1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 0, 1, 0, 0, 1, 0, 1},
                {0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 1, 1, 0, 1, 0, 1},
                {1, 1, 0, 1, 0, 0, 0, 0, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 1, 0, 0, 1, 1, 1},
                {1, 0, 0, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0, 0, 1, 0},
                {1, 1, 0, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 0},
                {0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 0, 1},
                {1, 1, 1, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1},
                {1, 0, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 0, 0, 1},
                {0, 0, 1, 1, 1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 1},
                {1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0},
                {1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1},
                {1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 0, 1},
                {1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 1},
                {1, 0, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 0, 0, 1, 1, 0, 1, 1, 1, 0, 0, 0, 1, 0, 1, 0, 0},
                {1, 0, 1, 1, 1, 0, 1, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 0, 1},
                {1, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 1, 1, 1},
                {1, 0, 1, 0, 1, 1, 1, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 0, 0, 0, 1},
                {1, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 0, 0, 1, 0, 0, 1, 1, 1, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0, 1},
                {0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1},
                {1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1},
                {1, 0, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1},
        };
        std::vector<Cube> cubes;
        for (int i = 0; i < map.size(); ++i) {
            for (int j = 0; j < map[i].size(); ++j) {
                if (map[i][j] == 1) {
                    Cube c(1, vec3{i, j, 0});
                    cubes.push_back(c);
                } else {
                    Cube c(1, vec3{i, j, -1});
                    cubes.push_back(c);
                }
            }
        }

        std::vector<vec3> points;
        std::vector<vec3> normals;
        std::vector<vec2> texture_coords;
        for (auto c : cubes) {
            for (auto rect : c.facets) {
                for (auto texture_coord: rect.getTextureCoords()) {
                    texture_coords.push_back(texture_coord);
                }
                for (auto p: rect.getPoints()) {
                    normals.push_back(rect.normal);
                    points.push_back(p);
                }
            }
        }

        assert (normals.size() == points.size());

        _buffer = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        _buffer->setData(sizeof(vec3) * points.size(), points.data());
        _mesh = std::make_shared<Mesh>();
        _mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, sizeof(vec3), 0, _buffer);
        _mesh->setPrimitiveType(GL_TRIANGLES);
        _mesh->setVertexCount(points.size());

        _normals_buffer = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        _normals_buffer->setData(sizeof(vec3) * normals.size(), normals.data());
        _mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, sizeof(vec3), 0, _normals_buffer);

        _texture_coords_buffer = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        _texture_coords_buffer->setData(sizeof(vec2) * texture_coords.size(), texture_coords.data());
        _mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, sizeof(vec2), 0, _texture_coords_buffer);

        _mesh->setModelMatrix(mat4(1.0));
        _texture = loadTexture("796NischimenkoData1/floor.jpg");

        glGenSamplers(1, &_sampler);

        glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_LINEAR);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_LINEAR);
        glSamplerParameterf(_sampler, GL_TEXTURE_MAX_ANISOTROPY_EXT, 16.0f);
    }

    std::shared_ptr<ShaderProgram> _shader;
    std::shared_ptr<Mesh> _mesh;
    DataBufferPtr _buffer;
    DataBufferPtr _normals_buffer;
    DataBufferPtr _texture_coords_buffer;
    std::vector<Triangle> triangles{};
    TexturePtr _texture;
    GLuint _sampler;
};

int main() {
    std::cout << "HElllo worldld";
    MyApplication app;
    app.start();
    return 0;
}